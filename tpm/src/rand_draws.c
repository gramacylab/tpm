/****************************************************************************
 *
 * Regularized logistic regression
 * Copyright (C) 2011, The University of Chicago
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 *
 * Questions? Contact Robert B. Gramacy (rbgramacy@chicagobooth.edu)
 *
 ****************************************************************************/


#include <stdlib.h>
#include <assert.h>
#include <R.h>
#include <Rmath.h>
#include "randomkit.h"
#ifdef _OPENMP
  #include <omp.h>
#endif 
#include "rand_draws.h"

int NS;
rk_state **states;


/* 
 * newRNGstates:
 * 
 * seeding (potentiall multiple) RNGs
 */

void newRNGstates(void)
{
  unsigned long s;
  int i;

#ifdef _OPENMP
  NS = omp_get_max_threads();
#else
  NS = 1;
#endif
  states = (rk_state**) malloc(sizeof(rk_state*) * NS);

  for(i=0; i<NS; i++) {
    states[i] = (rk_state*) malloc(sizeof(rk_state));
    s = 10000 * unif_rand();
    rk_seed(s, states[i]);
  }
}


/* 
 * deleteRNGstates:
 *
 * freeing (potentially multiple) RNGs
 */

void deleteRNGstates(void)
{
  unsigned int i;
  for(i=0; i<NS; i++) {
    free(states[i]);
  }
  free(states); 
  states = NULL;
  NS = 0;
}


/* 
 * runi:
 * 
 * one from a uniform(0,1)
 * from jenise 
 */

double runi(rk_state *state)
{
    unsigned long rv;
    assert(state);
    rv = rk_random((rk_state*) state);
    return ((double) rv) / RK_MAX;
}
