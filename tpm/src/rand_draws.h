#ifndef __RAND_DRAWS_H__
#define __RAND_DRAWS_H__

#include "randomkit.h"

void newRNGstates(void);
void deleteRNGstates(void);
double runi(rk_state *state);

#endif
