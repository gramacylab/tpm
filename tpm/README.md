## This file contains information on how to run TPMC codes (newer version)
    
- Compile the C code: in the terminal, under the directory of src, type: R CMD SHLIB -o tpm.so *.c 
  
- Source `tpm.R` using the appropriate path in the file to be used for TPMC simulation
  
- For non-parallel output collection, use the function called `tpm`. The following arguments should be provided:  
    + X: the parameter input space with appropriate names
    + moles: defines the species mole fractions (i.e. the ratio of the species number density to the total number density). Only six species are allowed: O (atomic oxygen), O<sub>2</sub> (diatomic oxygen), N (atomic nitrogen), N<sub>2</sub>(diatomic nitrogen), He (helium), and H (hydrogen).
    + stl: the Mesh file, which is under the ../Mesh_Files subdirectory: There are 10 mesh files for HST(HST_0-90.stl), while only one for GRACE (GRACE_A0_B0_ascii_redone.stl)
    + gsi=c("CLL", "DRIA", "Maxwell") by default
    + verb: 0: simulation progress will not be provided; 1: simulation progress will be provided

- The non-parallel chunk version, `tpm.chunk`, was designed to collect output for the parameter input space having the same mole fraction. It also can be used for the parallel collection, which works great.         
  
- For parallel output collection, use the function called `tpm.parallel`. Either via *sockets* or through *Rmpi*.  