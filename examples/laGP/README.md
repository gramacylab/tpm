## This directory contains code supporting the LANL satellite drag application. 

Except for **Figure 1**, all of the other figures and tables in the satellite drag paper *Emulating satellite drag from large simulation experiments*, by Sun, et al., (SIAM/ASA JUQ 2019), are generated from the files under this directory.  

### The contents of each file, and the corresponding figure(s) and table(s) generated, are detailed below:       
     
- `blhs_H-mc.R`: to accomplish *H* Monte Carlo Events for the HST_He (1 million). The purpose of this experiment is to compare the predictors based on the lengthscales estimated via random subsampling and BLHS, respectively. No figure provided in the paper, however it provided a critical building block for later developments.     
  
- `cv_boxplots.R`: to generate boxplots of satdrag_cv experiments, for both HST and GRACE. **Figures 10-12** are generated accordingly. *The data used here is too big to be in the repository, and thus, please contact Bobby Gramacy (rbg@vt.edu) or Furong Sun (furongs@vt.edu) for data when needed.*    
  
- `cv_snow.R`: to collect 10-fold cross validation experiments of each chemical species for both HST and GRACE, respectively, using 20 laGP comparators; includes both serial and parallel versions. `cv_boxplots.R` visualizes the results produced by this file.     
  
- `cv_snow.sbatch`: the job submission bash scripts for SLURM-managed *midway* cluster operated by the Research Computing Center at the University of Chicago.   
  
- `cv-test_snow.qsub`: the job submission bash scripts for torque-managed *newriver* cluster operated by the Advanced Research Computing facility at Virginia Tech.

- `dalc.Rmd`: derivation of pathwise *active learning Cohn (ALC)* with respect to new design point, ``to-be-swapped-in point", including both isotropic and separable versions. 

- `function.R`: contains test functions (2d and 4d) and the score metric evaluation support (Mahalanobis distance calculation included) used by other files.

- `michalewicz.R`: to compare the predictor's performance based on random subsampling and BLHS using simulated data generated from the Michalewicz function under varied dimensions; **Figures 4,9** and **Tables 3-4**.   

- `path.R`: to compare the local designs under ``jALC(exh)", ``jALC(opt)", and ``jNN", respectively; to generate predictive draws based on joint sampling; **Figure 5**.  

- `path_random.R`: to collect RMSE and Mahalanobis distance based on 100 randomly generated lines; **Figure 7**.

- `path_time.R`: to collect RMSE and Mahalanobis distance versus time, respectively, using the local design with increasing size under different criteria; **Figure 6**. 

- `randline.R`: a generic function to generate random lines in the 2d input space; **Figure 15**.  

- `synthetic.R`: to collect 30 Monte Carlo events using simulated data from borehole and Michalewicz function, respectively, based on 20 laGP comparators; **Figure 8**.

- `test.R`: contains a fitting and predicting function to duplicate proof-of-concept results in Mehta, et.al., 2014, the original satellite drag emulation paper.
  
- `test_rmspe.R`: to calculate RMSE and RMSPE using the ensemble predictor, for HST and GRACE, respectively; the RMSPE result is shown in Section 6.1. *The data used here is too big to be in the repository, and thus, please contact Bobby Gramacy (rbg@vt.edu) or Furong Sun (furongs@vt.edu) for data when necessary.*    
  
- `test_snow.R`: to collect predictive means for an ensemble testing set using training sets from each chemical species; `test_rmspe.R` visualizes the results produced by this file.  

- `untra.R`: TPMC simulation ("../../tpm/R/tpm.R") over 4 trajectories: GRACE-quiet, GRACE-active, HST-quiet, and HST-active.

- `uq_traj.R`: to compare the predictor's performance using different subsampling strategies (joint vs pointwise) and varied criteria (ALC vs NN); **Figures 13-14** and **Table 6**.

- `visual.R`: to generate 2d visualization of the subsample via BLHS and random subsampling, respectively; also to generate the distribution of lengthscale MLE from simulated data based on borehole function and HST_He (1 million), respectively; **Figures 1-2**.

- `compare.R`: to compare *laGP*'s performance with *RobustGaSP* and *DiceKriging* upon request of the referee.
