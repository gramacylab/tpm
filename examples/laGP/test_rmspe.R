## This file is for calculating RMSE and RMSPE using the ensemble predictor, for HST and GRACE, respectively.
## ***The data used here is too big to be in the repository, and thus, please contact 
## Bobby Gramacy (rbg@vt.edu) or Furong Sun (furongs@vt.edu) for data when necessary.***

load("sat_hstEns_laGP.RData") ## HST
## load("sat_graceEns_laGP.RData") ## GRACE
tt <- read.table("../../data/HST/hstEns.dat", header=TRUE) ## HST
## tt <- read.table("../../data/GRACE/graceEns.dat", header=TRUE) ## GRACE

mf <- c(0.83575679477, 0.00004098807, 0.01409589809, 0.00591827778, 0.13795985368, 0.00622818760)
pm <- c(2.65676, 5.31352, 2.32586, 4.65173, 0.665327, 0.167372)
w <- mf * pm

yy.alc2.sb <- as.matrix(YY.alc2.sb) %*% w / sum(w)
rmse_alc2.sb <- sqrt(mean((yy.alc2.sb - tt$Cd)^2))
rmspe_alc2.sb <- sqrt(mean((100*(yy.alc2.sb - tt$Cd)/tt$Cd)^2))

yy.subb <- as.matrix(YY.subb) %*% w / sum(w)
rmse_subb <- sqrt(mean((yy.subb - tt$Cd)^2))
rmspe_subb <- sqrt(mean((100*(yy.subb - tt$Cd)/tt$Cd)^2))

yy.alc2.s <- as.matrix(YY.alc2.s) %*% w / sum(w)
rmse_alc2.s <- sqrt(mean((yy.alc2.s - tt$Cd)^2))
rmspe_alc2.s <- sqrt(mean((100*(yy.alc2.s - tt$Cd)/tt$Cd)^2))

yy.sub <- as.matrix(YY.sub) %*% w / sum(w)
rmse_sub <- sqrt(mean((yy.sub - tt$Cd)^2))
rmspe_sub <- sqrt(mean((100*(yy.sub - tt$Cd)/tt$Cd)^2))

RMSE <- data.frame(cbind(rmse_sub, rmse_alc2.s, rmse_subb, rmse_alc2.sb))
names(RMSE) <- c("sub", "alc2.s", "subb", "alc2.sb")

RMSPE <- data.frame(cbind(rmspe_sub, rmspe_alc2.s, rmspe_subb, rmspe_alc2.sb))
names(RMSPE) <- names(RMSE)
