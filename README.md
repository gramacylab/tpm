## This directory contains data and examples from the *satdrag* project, formerly residing in the `laGP` mercurial repo.

Maintainers: Robert B. Gramacy (Virginia Tech) and Furong Sun (Virginia Tech)

- This work is in collaboration with Ben Haaland (University of Utah), Earl Lawrence and Andrew Walker (Los Alamos National Laboratory).
- For more details, see papers/citations listed below.

### A brief description of the contents of each sub-directory follows.

- `data`: the datasets
  
    + GRACE: TPMC simulation input and output for pointwise and trajectory experiments (Sections 6.1-2 in the paper linked above) for the Gravity Recovery and Climate Experiment satellite
    + HST: ditto for the Hubble Space Telescope satellite
    + ISS: TPMC simulation output for the International Space Station satellite
    + trajectory: the script/code to combine the parameters with TPMC output (HST and GRACE) and other information related to trajectory experiments (Section 6.2 in the paper linked above); See README.md therein for details
    
- `tpm`: the mesh files related to GRACE and HST; C and R codes supporting TPMC simulation; See README.md therein for details on compilation and execution

- `examples/laGP`: the code, particularly the one generating most of the figures in the paper linked above; See README.md therein for details
- `examples/design`: deprecated code generating designs for the HST and GRACE experiments, with follow-up code to check for *NAs* and re-run

## References

For more information, and for citation consider the following ...

[Sun, et al. (2019)](https://epubs.siam.org/doi/abs/10.1137/18M1170157) paper on `laGP` surrogate modeling of large simulation campaigns (millions od runs).  For citation use ...


```
@article{sun2019emulating,
  title={Emulating satellite drag from large simulation experiments},
  author={Sun, F and Gramacy, RB and Haaland, B and Lawrence, E and Walker, A},
  journal={SIAM/ASA Journal on Uncertainty Quantification},
  volume={7},
  number={2},
  pages={720--759},
  note={preprint ar{X}iv:1712.00182},
  year={2019}
}
``` 

The first paper with this simulator, by [Mehta, et al. (2014)](https://www.sciencedirect.com/science/article/pii/S027311771400413X) used thousands or runs.  For citation use ...


```
@article{mehta2014modeling,
  title={Modeling satellite drag coefficients with response surfaces},
  author={Mehta, PM and Walker, A and Lawrence, E and Linares, R and Higdon, D and Koller, J},
  journal={Advances in Space Research},
  volume={54},
  number={8},
  pages={1590--1607},
  year={2014},
  publisher={Elsevier}
}

```

For narrative that combines both papers, see Chapters 2 and 9 of [Gramacy (2019)](https://bobby.gramacy.com/surrogates/).  For citation use ..


```
@book{gramacy2020surrogates,
  title = {Surrogates: {G}aussian Process Modeling, Design and Optimization for the Applied Sciences},
  author = {Robert B. Gramacy},
  publisher = {Chapman Hall/CRC},
  address = {Boca Raton, Florida},
  note = {\url{http://bobby.gramacy.com/surrogates/}},
  year = {2020}
}
```
