import os
import numpy as np
import matplotlib.pyplot as plt

# Change the filename to the desired file
filename = 'GRACE_active.dat'

basepath = '/home/awalker/projects/impact/impact-prop/data'
filepath = os.path.join(basepath, filename)

# Open and read the file
with open(filepath, 'r') as f:
    lines = [line for line in f if line[0] != 'T']

# Create the empty arrays
nlines = len(lines)
data_dict = {}
var_names = ['time', 'x', 'y', 'z', 'u', 'v', 'w', 'yaw', 'pitch',
             'alpha_n', 'sigma_t', 'T_a', 'T_s', 'Vmag', 'X_O',
             'X_O2', 'X_N', 'X_N2', 'X_He', 'X_H']
for name in var_names:
    data_dict[name] = np.zeros(nlines)

# Parse the lines
for iline, line in enumerate(lines):
    data = line.strip().split()
    for iname, name in enumerate(var_names):
        data_dict[name][iline] = data[iname]

# Plot the data
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)

ax1.plot(data_dict['time'], data_dict['x'])
ax1.plot(data_dict['time'], data_dict['y'])
ax1.plot(data_dict['time'], data_dict['z'])

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(data_dict['time'], data_dict['u'])
ax2.plot(data_dict['time'], data_dict['v'])
ax2.plot(data_dict['time'], data_dict['w'])

fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
ax3.plot(data_dict['time'], data_dict['alpha_n'])

fig4 = plt.figure()
ax4 = fig4.add_subplot(111)
ax4.plot(data_dict['time'], data_dict['T_a'])
ax4.plot(data_dict['time'], data_dict['T_s'])

fig5 = plt.figure()
ax5 = fig5.add_subplot(111)
ax5.plot(data_dict['time'], data_dict['Vmag'])

fig6 = plt.figure()
ax6 = fig6.add_subplot(111)
ax6.plot(data_dict['time'], data_dict['X_O'])
ax6.plot(data_dict['time'], data_dict['X_O2'])
ax6.plot(data_dict['time'], data_dict['X_N'])
ax6.plot(data_dict['time'], data_dict['X_N2'])
ax6.plot(data_dict['time'], data_dict['X_He'])
ax6.plot(data_dict['time'], data_dict['X_H'])

data_dict['radius'] = np.zeros(nlines)
for itime in range(len(data_dict['time'])):
    pos_vec = np.array([data_dict['x'][itime],
                        data_dict['y'][itime],
                        data_dict['z'][itime]])
    data_dict['radius'][itime] = np.linalg.norm(pos_vec)

fig7 = plt.figure()
ax7 = fig7.add_subplot(111)
ax7.plot(data_dict['time'], data_dict['radius'])

plt.show()
