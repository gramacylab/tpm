The data sets (.dat) and propagator input files (.inp) are included for a total
of four experiments using the GRACE and HST satellite geometries. The
experiments are labeled:

1) GRACE active
2) GRACE quiet
3) HST active
4) HST quiet

The GRACE results correspond to an orbit representative of GRACE's orbit in
2017. The Keplerian orbital elements were taken from July 31st 2017 and
are:

Semi-Major Axis (km)                                   # 6713.0
Eccentricity                                           # 0.0015
Inclination (degrees)                                  # 89.0
Argument of Perigee (degrees)                          # 95.0
Right-Ascension of the Ascending Node (degrees)        # 314.0
True Anomaly (degrees)                                 # 275.0

Similarly, the HST results correspond to an orbit representative of HST's orbit
in 2017. The Keplerian orbital elements were taken from October 11, 2017 and
are:

Semi-Major Axis (km)                                   # 6918.0
Eccentricity                                           # 0.00027
Inclination (degrees)                                  # 28.5
Argument of Perigee (degrees)                          # 350.0
Right-Ascension of the Ascending Node (degrees)        # 318.0
True Anomaly (degrees)                                 # 125.0

All simulations are performed arbitrarily for the day of Jan. 1st 2017 at
00:00:00 UTC for a full day (86400 seconds). No attempt was made to align
the orbital phase of each satellite; these are only meant to be example
positions.

The difference between active and quiet periods is the setting for the
daily geomagnetic index, ap:

Quiet = 5.0
Active = 200.0

In all cases, the solar flux index (daily and 81-day averaged) is set to 150.0.
The atmospheric parameters are computed using the NRLMSISE-00 model using
the F10.7 and ap inputs specified.